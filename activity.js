// Single Room
db.collections.insert({
	name: "single",
	accomodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	rooms_available: 10,
	isAvailable: false
})
// Single Room END
// Multiple Rooms
db.collections.insertMany([
	{
		name: "double",
		accomodates: 3,
		price: 2000,
		description: "A room fit for a small family going on a vacation",
		rooms_available: 5,
		isAvailable: false
	},
	{
		name: "queen",
		accomodates: 4,
		price: 4000,
		description: "A room with a queen sized bed perfect for a simple getaway",
		rooms_available: 15,
		isAvailable: false
	},
])
// Multiple Rooms END

// Find Method
db.collections.find({ name: "double" })
// Find Method END

// UpdateOne Method
db.collections.updateOne(
	{
		name: "queen"
	},
	{
		$set: {
			rooms_available: 0
		}
	}
)
// UpdateOne Method END

// [Delete Many]
db.collections.deleteMany({
	rooms_available: 0
})
// [Delete Many END]